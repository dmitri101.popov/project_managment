from settings import db
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.dialects.postgresql import ARRAY
import uuid


class Users(db.Model):
    __tablename__ = 'users'
    user_id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String)


class Project(db.Model):
    __tablename__ = 'projects'

    project_id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String)
    goal = db.Column(db.String)
    members = db.Column(ARRAY(UUID(as_uuid=True)))
    platform = db.Column(db.String)
    contact_person = db.Column(UUID(as_uuid=True))
    news = db.relationship('News', backref='project', cascade='all, delete-orphan')
    versions = db.relationship('Version', backref='project', cascade='all, delete-orphan')
    bugs = db.relationship('Bug', backref='project', cascade='all, delete-orphan')

class News(db.Model):
    __tablename__ = 'news'

    news_id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project_id = db.Column(UUID(as_uuid=True), db.ForeignKey('projects.project_id'), default=uuid.uuid4)
    title = db.Column(db.String)
    content = db.Column(db.Text)
    timestamp = db.Column(db.DateTime)

class Version(db.Model):
    __tablename__ = 'versions'

    version_id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project_id = db.Column(UUID(as_uuid=True), db.ForeignKey('projects.project_id'), default=uuid.uuid4)
    version_number = db.Column(db.String)
    release_date = db.Column(db.DateTime)
    description = db.Column(db.Text)
    documentation = db.relationship('Documentation', backref='version', cascade='all, delete-orphan')

class Documentation(db.Model):
    __tablename__ = 'documentation'

    doc_id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    version_id = db.Column(UUID(as_uuid=True), db.ForeignKey('versions.version_id'), default=uuid.uuid4)
    title = db.Column(db.String)
    content = db.Column(db.Text)
    files = db.Column(ARRAY(db.Text), default=[])
    
class Bug(db.Model):
    __tablename__ = 'bugs'

    bug_id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project_id = db.Column(UUID(as_uuid=True), db.ForeignKey('projects.project_id'), default=uuid.uuid4)
    version_id = db.Column(UUID(as_uuid=True), db.ForeignKey('versions.version_id'), default=uuid.uuid4)
    title = db.Column(db.String)
    description = db.Column(db.Text)
    status = db.Column(db.String)
