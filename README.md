# project_managment

## Запуск


```
sudo docker-compose up --build
```
 + когда вырубаешь не забывай про 
 ```
    sudo docker-compose down
 ```

или

```
 git clone 
 python3 -m venv venv
 source ./venv/bin/activate
 pip3 install -r requirements.txt
 python3 app.py
```


## миграции
если запускаешь через докер компост то забей

1. удаляешь migrations
2. создаешь в постгресе бд project

```
flask db init
flask db migrate
flask db upgrade
```


## База данных
### Таблица "projects"
* project_id (Integer): Уникальный идентификатор проекта.
* name (String): Название проекта.
* goal (String): Цель проекта.
* members (String): Состав участников проекта.
* platform (String): Платформа проекта.
* contact_person (String): Контактное лицо проекта.
* news (Relation): Связь с таблицей "news". Список новостей, связанных с проектом.
* versions (Relation): Связь с таблицей "versions". Список версий разрабатываемой программы.
* bugs (Relation): Связь с таблицей "bugs". Список ошибок, связанных с проектом.
### Таблица "news"
* news_id (Integer): Уникальный идентификатор новости.
* project_id (Integer): Идентификатор проекта, к которому относится новость.
* title (String): Заголовок новости.
* content (Text): Содержание новости.
* timestamp (DateTime): Временная метка создания новости.
### Таблица "versions"
* version_id (Integer): Уникальный идентификатор версии.
* project_id (Integer): Идентификатор проекта, к которому относится версия.
* version_number (String): Номер версии.
* release_date (DateTime): Дата выпуска версии.
* description (Text): Описание версии.
* documentation (Relation): Связь с таблицей "documentation". Список документов, связанных с версией.
### Таблица "documentation"
* doc_id (Integer): Уникальный идентификатор документации.
* version_id (Integer): Идентификатор версии, к которой относится документация.
* title (String): Заголовок документа.
* content (Text): Содержание документа.
### Таблица "bugs"
* bug_id (Integer): Уникальный идентификатор ошибки.
* project_id (Integer): Идентификатор проекта, к которому относится ошибка.
* version_id (Integer): Идентификатор версии, к которой относится ошибка.
* title (String): Заголовок ошибки.
* description (Text): Описание ошибки.
* status (String): Статус ошибки.

## Маршруты

### GET /projects/<project_id>/details
#### Описание: Получить информацию о проекте со всеми связанными данными.
Параметры пути:
* project_id: Идентификатор проекта.

Коды ответов:
* 200 OK: Успешный запрос. Возвращает информацию о проекте в формате JSON.
* 404 Not Found: Проект не найден.

### GET /projects
#### Описание: Получить список всех проектов.
#### Коды ответа:
* 200 OK: Запрос выполнен успешно.
#### Возвращаемые данные:
Список проектов с полями:
* project_id (Integer)
* name (String)
* goal (String)
* members (String)
* platform (String)
* contact_person (String)

### POST /projects
#### Описание: Создать новый проект.
#### Коды ответа:
* 200 OK: Проект успешно создан.
#### Поля, передаваемые в теле запроса:
* name (String)
* goal (String)
* members (String)
* platform (String)
* contact_person (String)
### GET /projects/{project_id}/news
#### Описание: Получить список всех новостей для указанного проекта.
#### Коды ответа:
* 200 OK: Запрос выполнен успешно.
* 404 Not Found: Проект не найден.
#### Возвращаемые данные:
Список новостей с полями:
* news_id (Integer)
* title (String)
* content (Text)
* timestamp (DateTime)
### POST /projects/{project_id}/news
#### Описание: Создать новую новость для указанного проекта.
#### Коды ответа:
* 200 OK: Новость успешно создана.
* 404 Not Found: Проект не найден.
#### Поля, передаваемые в теле запроса:
* title (String)
* content (Text)
* timestamp (DateTime)
### GET /projects/{project_id}/versions
#### Описание: Получить список всех версий для указанного проекта.
#### Коды ответа:
* 200 OK: Запрос выполнен успешно.
* 404 Not Found: Проект не найден.
#### Возвращаемые данные:
Список версий с полями:
* version_id (Integer)
* version_number (String)
* release_date (DateTime)
* description (Text)
### POST /projects/{project_id}/versions
#### Описание: Создать новую версию для указанного проекта.
#### Коды ответа:
* 200 OK: Версия успешно создана.
* 404 Not Found: Проект не найден.
#### Поля, передаваемые в теле запроса:
* version_number (String)
* release_date (DateTime)
* description (Text)
### GET /projects/{project_id}/versions/{version_id}
#### Описание: Получить информацию о конкретной версии для указанного проекта.
#### Коды ответа:
* 200 OK: Запрос выполнен успешно.
* 404 Not Found: Проект или версия не найдены.
#### Возвращаемые данные:
Информация о версии с полями:
* version_id (Integer)
* version_number (String)
* release_date (DateTime)
* description (Text)
### POST /projects/{project_id}/versions/{version_id}/documentation
#### Описание: Создать новый документ для указанной версии проекта.
#### Коды ответа:
200 OK: Документ успешно создан.
404 Not Found: Проект или версия не найдены.
#### Поля, передаваемые в теле запроса:
* title (String)
* content (Text)
### GET /projects/{project_id}/versions/{version_id}/bugs
#### Описание: Получить список всех ошибок для указанного проекта и версии.
#### Коды ответа:
* 200 OK: Запрос выполнен успешно.
* 404 Not Found: Проект или версия не найдены.
#### Возвращаемые данные:
Список ошибок с полями:
* bug_id (Integer)
* title (String)
* description (Text)
* status (String)
### POST /projects/{project_id}/versions/{version_id}/bugs
#### Описание: Создать новую ошибку для указанного проекта и версии.
#### Коды ответа:
* 200 OK: Ошибка успешно создана.
* 404 Not Found: Проект или версия не найдены.
#### Поля, передаваемые в теле запроса:
* title (String)
* description (Text)
* status (String)
* ### GET /projects/{project_id}/versions/{version_id}/documentation
#### Описание: Получить список всех документов для указанной версии проекта.
#### Коды ответа:
* 200 OK: Запрос выполнен успешно.
* 404 Not Found: Проект или версия не найдены.
#### Возвращаемые данные:
Список документов с полями:
* doc_id (Integer)
* title (String)
* content (Text)
### GET /projects/{project_id}/versions/{version_id}/documentation/{doc_id}
#### Описание: Получить информацию о конкретном документе для указанной версии проекта.
#### Коды ответа:
* 200 OK: Запрос выполнен успешно.
* 404 Not Found: Проект, версия или документ не найдены.
#### Возвращаемые данные:
Информация о документе с полями:
* doc_id (Integer)
* title (String)
* content (Text)
### GET /projects/{project_id}/bugs
#### Описание: Получить список всех ошибок для указанного проекта.
#### Коды ответа:
* 200 OK: Запрос выполнен успешно.
* 404 Not Found: Проект не найден.
#### Возвращаемые данные:
Список ошибок с полями:
* bug_id (Integer)
* title (String)
* description (Text)
* status (String)
### POST /projects/{project_id}/bugs
#### Описание: Создать новую ошибку для указанного проекта.
#### Коды ответа:
* 200 OK: Ошибка успешно создана.
* 404 Not Found: Проект не найден.
#### Поля, передаваемые в теле запроса:
* title (String)
* description (Text)
* status (String)