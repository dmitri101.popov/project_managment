FROM python:3.11.3-slim-buster

# set work directory
WORKDIR /code


# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /code/requirements.txt
RUN pip install -r requirements.txt

# copy project
COPY . /code/

COPY ./wtf.sh /wtf.sh
ENTRYPOINT ["sh", "/wtf.sh"]
