from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from settings import app, db
import routes



migrate = Migrate(app, db)
app.register_blueprint(routes.bp)


if __name__ == '__main__':
    
    app.run(debug=False, port=5005)
