import json, datetime, time
from typing import List
from fake_data_generator.users_api_repository.data_structures import UsersCreatePOST

from fake_data_generator.api.api import ApiSync


class UsersRepositoryRealizationAPI():
    def __init__(self) -> None:
        __connection_data = self.__get_connection_data()
        self.url = __connection_data["USERS_URL"]
        self.api = ApiSync()

    def __get_connection_data(self):
        with open("fake_data_generator/config.json", "r") as f:
            connection_data = json.loads(f.read())
        return connection_data

    def create_user(self, data: UsersCreatePOST, token:str=None):
        # try:
            
            final_url_path = self.url

            response = self.api.post(
                url_path=final_url_path, data=data.json(), auth_token=token
            )

        # except:
        #     print("create user error")
    
    def get_all_users(self, token:str=None):
        try:
            final_url_path = self.url

            response = self.api.get(
                url_path=final_url_path,  auth_token=token
            )

            return response.text
        except:
            print("create user error")
