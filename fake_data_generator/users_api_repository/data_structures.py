from pydantic import BaseModel


class UsersCreatePOST(BaseModel):
    name: str = None


class UsersGET(BaseModel):
    name: str = None
    user_id: str = None
