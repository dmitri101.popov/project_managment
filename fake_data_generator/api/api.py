import json, requests


class ApiSync:
    def __init__(self) -> None:
        self.timeout = 5
        self.__connection_data = self.__get_connection_data()
        self.__base_url = self.__connection_data["API_URL"]
        
        self.post_session_args = {
            "Content-Type": "application/json",
        }
        self.get_session_args = { 
            "accept": "application/json"
        }
        self.put_session_args = {
            "Content-Type": "application/json",
        }
        self.delete_session_args = {
            "Content-Type": "application/json",
        }
    

    def __get_connection_data(self):
        try:
            with open("fake_data_generator/config.json", "r") as f:
                connection_data = json.loads(f.read())
                return connection_data
        except:
            print("error - __get_connection_data")

    def get(self, url_path: str, params=None, auth_token=None):
        try:
            url = self.__base_url + url_path
            
            if auth_token:
                self.get_session_args["authorization"] = "Bearer " + auth_token

            output = requests.get(
                url, params=params, timeout=self.timeout, headers=self.get_session_args
            )
           
            return output
        except:
            print("error - get")

    def post(self, url_path: str, data=None, files=None,  auth_token: str=None):
        try:
            url = self.__base_url + url_path
            
            if auth_token:
                self.post_session_args["authorization"] = "Bearer " + auth_token
                
            if data:
                output = requests.post(
                    url, timeout=self.timeout, headers=self.post_session_args, data=data
                )

            elif files:
                output = requests.post(
                    url, timeout=self.timeout, files=files
                )


            if output:
                return output
        except:
            print("error - post")

    def put(self, url_path: str, data):
        try:
            url = self.__base_url + url_path
            output = requests.put(url=url, data=data, timeout=self.timeout, headers=self.put_session_args)
        
            return output
        except:
            print("error - put")

    def delete(self, url_path: str, data):
        try:
            url = self.__base_url + url_path
            output = requests.delete(url=url, data=data, timeout=self.timeout, headers=self.delete_session_args)
          
            return output
        except:
            print("error - delete")
