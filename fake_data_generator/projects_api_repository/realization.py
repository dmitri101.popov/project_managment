import json, datetime, time
from typing import List
from fake_data_generator.projects_api_repository.data_structures import *

from fake_data_generator.api.api import ApiSync


class ProjectsRepositoryRealizationAPI():
    def __init__(self) -> None:
        __connection_data = self.__get_connection_data()
        self.url = __connection_data["PROJECTS_URL"]
        self.api = ApiSync()

    def __get_connection_data(self):
        with open("fake_data_generator/config.json", "r") as f:
            connection_data = json.loads(f.read())
        return connection_data

    def create_project(self, data: ProjectsPOST, token:str=None):
        try:
            
            final_url_path = self.url

            response = self.api.post(
                url_path=final_url_path, data=data.json(), auth_token=token
            )

            return response.text
        except:
            print("create project error")
    
    def create_news(self, project_id: str, data: ProjectsNewsPOST, token:str=None):
        try:
            
            final_url_path = self.url + '/' + project_id + '/news'

            response = self.api.post(
                url_path=final_url_path, data=data.json(), auth_token=token
            )

            return response.text
        except:
            print("create news error")
    
    def create_version(self, project_id: str, data: ProjectsVersionsPOST, token:str=None):
        try:
            
            final_url_path = self.url + '/' + project_id + '/versions'

            response = self.api.post(
                url_path=final_url_path, data=data.json(), auth_token=token
            )

            return response.text
        except:
            print("create version error")


    def create_bugs(self, project_id: str, version_id: str, data: ProjectsBugsPOST, token:str=None):
        try:
            
            final_url_path = self.url + '/' + project_id + '/versions/' + version_id + '/bugs'

            response = self.api.post(
                url_path=final_url_path, data=data.json(), auth_token=token
            )

            return response.text
        except:
            print("create bugs error")


    def create_documentation(self, project_id: str, version_id: str, data: ProjectsDocumentationPOST, token:str=None):
        try:

            final_url_path = self.url + '/' + project_id + '/versions/' + version_id + '/documentation'

            response = self.api.post(
                url_path=final_url_path, data=data.json(), auth_token=token
            )

            return response.text
        except:
            print("create documentation error")


    def create_documentation_file(self, doc_id: str, file, token:str=None):
        try:
            
            final_url_path = self.url + '/versions/' + 'documentation/' + doc_id + '/file'

            response = self.api.post(
                url_path=final_url_path, auth_token=token, files={'file': file}
            )

            if response:
                print(response.text)
                return response.text
        except:
            print("create documentation error")


    def get_all_projects(self, token:str=None):
        try:
            final_url_path = self.url

            response = self.api.get(
                url_path=final_url_path,  auth_token=token
            )

            return response.text
        except:
            print("get project error")


    def get_all_versions(self, project_id: str, token:str=None):
        try:
            final_url_path = self.url + '/' + project_id + '/versions'

            response = self.api.get(
                url_path=final_url_path,  auth_token=token
            )

            return response.text
        except:
            print("get project error")
    

    def get_all_docs(self, project_id: str, version_id: str, token:str=None):
        try:
            final_url_path = self.url + '/' + project_id + '/versions/' + version_id + '/documentation'

            response = self.api.get(
                url_path=final_url_path,  auth_token=token
            )

            return response.text
        except:
            print("get project error")