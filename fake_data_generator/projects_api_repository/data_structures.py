from pydantic import BaseModel
from typing import List
from fake_data_generator.users_api_repository.data_structures import UsersGET





class ProjectsPOST(BaseModel):
    name: str = None
    goal: str = None
    members: List[str] = None
    platform: str = None
    contact_person: str = None


class ProjectsGet(ProjectsPOST):
    members: List[UsersGET] = None
    project_id: str = None
    contact_person: UsersGET = None

class ProjectsNewsPOST(BaseModel):
    title: str = None
    content: str = None
    timestamp: str = None


class ProjectsVersionsPOST(BaseModel):
    version_number: str = None
    release_date: str = None
    description: str = None


class VersionsGet(ProjectsVersionsPOST):
    version_id: str = None


class ProjectsBugsPOST(BaseModel):
    title: str = None
    description: str = None
    status: str = None


class ProjectsDocumentationPOST(BaseModel):
    title: str = None
    content: str = None


class ProjectsDocumentationGET(ProjectsDocumentationPOST):
    doc_id: str = None
