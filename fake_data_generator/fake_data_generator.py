import json, random, datetime, requests
from dateutil import parser
from bs4 import BeautifulSoup

from fake_data_generator.users_api_repository.output import users_repository_realization_api
from fake_data_generator.users_api_repository.data_structures import *

from fake_data_generator.projects_api_repository.output import projects_repository_realization_api
from fake_data_generator.projects_api_repository.data_structures import *

from faker import Faker


fake = Faker()


def generateUser() -> UsersGET:
    test_user = UsersCreatePOST(name=fake.name())
    users_repository_realization_api.create_user(test_user)

    all_users = users_repository_realization_api.get_all_users()


    all_users = json.loads(all_users)

    rand_user = UsersGET.parse_obj(random.choice(all_users))
    return rand_user

def generateProject() -> ProjectsGet:
    all_users = users_repository_realization_api.get_all_users()
    all_users = json.loads(all_users)
    all_users_parsed = []

    for unparsed_user in all_users:
        all_users_parsed.append(UsersGET.parse_obj(unparsed_user))
    
    chosen_users = random.sample(all_users_parsed, random.randint(1, len(all_users_parsed)))
    members = [usr.user_id for usr in chosen_users]


    test_project = ProjectsPOST(name=fake.domain_word(),
                                goal=fake.paragraph(),
                                members=members,
                                platform=fake.file_extension(category='text'),
                                contact_person=random.choice(members))
    
    projects_repository_realization_api.create_project(test_project)
    all_projects = projects_repository_realization_api.get_all_projects()
    all_projects = json.loads(all_projects)

    rand_project = ProjectsGet.parse_obj(random.choice(all_projects))
    return rand_project


def generateNews(rand_project: ProjectsGet):
    fake_time = fake.date_time_this_century()
    fake_time = datetime.datetime.isoformat(fake_time)

    fake_news = ProjectsNewsPOST(title=fake.text(max_nb_chars=20),
                                 content=fake.text(max_nb_chars=180),
                                 timestamp=fake_time)
    projects_repository_realization_api.create_news(project_id=rand_project.project_id, data=fake_news)


def generateVersions(rand_project: ProjectsGet):
    fake_time = fake.date_time_this_century()
    fake_time = datetime.datetime.isoformat(fake_time)

    fake_version = ProjectsVersionsPOST(version_number=str(random.randint(0, 20)) + '.' + str(random.randint(0, 20)),
                         release_date=fake_time,
                         description=fake.text(max_nb_chars=20))

    projects_repository_realization_api.create_version(project_id=rand_project.project_id, data=fake_version)


    project_versions = projects_repository_realization_api.get_all_versions(project_id=rand_project.project_id)
    project_versions = json.loads(project_versions)
    rand_version = VersionsGet.parse_obj(random.choice(project_versions))

    return rand_version


def generateBugs(rand_project: ProjectsGet, rand_version: VersionsGet):
    fake_bug = ProjectsBugsPOST(title=fake.text(max_nb_chars=20),
                                description=fake.text(max_nb_chars=50),
                                status=fake.text(max_nb_chars=10))
    
    projects_repository_realization_api.create_bugs(project_id=rand_project.project_id, version_id=rand_version.version_id, data=fake_bug)


def generateDocumentation(rand_project: ProjectsGet, rand_version: VersionsGet):

    fake_documentation = ProjectsDocumentationPOST(title=fake.text(max_nb_chars=20),
                                                   content=fake.text(max_nb_chars=150))

    projects_repository_realization_api.create_documentation(project_id=rand_project.project_id, version_id=rand_version.version_id, data=fake_documentation)
    
    project_docs = projects_repository_realization_api.get_all_docs(project_id=rand_project.project_id, version_id=rand_version.version_id)
    project_docs = json.loads(project_docs)
    rand_doc = ProjectsDocumentationGET.parse_obj(random.choice(project_docs))

    return rand_doc


def generateFile(rand_document: ProjectsDocumentationGET):
    data = requests.get('http://www.4geeks.de/cgi-bin/webgen.py')
    parsed = BeautifulSoup(data.text)
    program_text = parsed.find('pre').getText()

    with open('test_program.py', 'w') as f:
        f.write(program_text)

    projects_repository_realization_api.create_documentation_file(doc_id=rand_document.doc_id, file=open('test_program.py', 'rb'))
