from werkzeug.datastructures import FileStorage
from settings import db
from flask import jsonify, request, Blueprint
from models import Project, News, Version, Documentation, Bug, Users
from flask_restx import Api, Resource, fields
from dateutil.parser import parse
import pickle
from datetime import datetime
from sqlalchemy.orm.attributes import flag_modified
from flask import send_file
import uuid
import os


from fake_data_generator.fake_data_generator import *


bp = Blueprint('routes', __name__)
swagger_api = Api(bp)


users_post = swagger_api.model('users_post', {
    'name': fields.String(required=True, description=''),
})


@swagger_api.route('/users/<uuid:user_id>')
class UserInfo(Resource):
    def get(self, user_id):
        user = Users.query.get(user_id)

        if user is None:
            return jsonify({'message': 'User not found'})

        user_data = {
            'user_id': user.user_id,
            'name': user.name,
        }

        return jsonify(user_data)


@swagger_api.route('/users')
class UsersInfo(Resource):
    def get(self):
        users = Users.query.all()

        user_list = []
        for user in users:
            user_data = {
                'user_id': user.user_id,
                'name': user.name,
            }
            user_list.append(user_data)

        return jsonify(user_list)

    # Маршрут для создания нового проекта
    @swagger_api.expect(users_post)
    def post(self):
        user_data = request.get_json()
        project = Users(
            name=user_data['name']
        )
        db.session.add(project)
        db.session.commit()
        return jsonify({'message': 'User created successfully'})


projects_post = swagger_api.model('projects_post', {
    'name': fields.String(required=True, description=''),
    'goal': fields.String(required=True, description=''),
    'members': fields.List(fields.String(required=True, description='')),
    'platform': fields.String(required=True, description=''),
    'contact_person': fields.String(required=True, description=''),
})


# Маршрут для получения списка всех проектов

@swagger_api.route('/projects')
class ProjectsInfo(Resource):
    def get(self):
        projects = Project.query.all()

        project_list = []
        for project in projects:

            user_infos = []

            for user_id in project.members:
                user = Users.query.get(user_id)
                user_infos.append({'user_id': user.user_id,
                                   'name': user.name})

            contact_person_info = Users.query.get(project.contact_person)

            project_data = {
                'project_id': project.project_id,
                'name': project.name,
                'goal': project.goal,
                'members': user_infos,
                'platform': project.platform,
                'contact_person': {'user_id': contact_person_info.user_id,
                                   'name': contact_person_info.name}
            }
            project_list.append(project_data)

        return jsonify(project_list)

    # Маршрут для создания нового проекта
    @swagger_api.expect(projects_post)
    def post(self):
        project_data = request.get_json()

        project = Project(
            name=project_data['name'],
            goal=project_data['goal'],
            members=project_data['members'],
            platform=project_data['platform'],
            contact_person=project_data['contact_person']
        )
        db.session.add(project)
        db.session.commit()
        return jsonify({'message': 'Project created successfully'})


@swagger_api.route('/projects/<uuid:project_id>')
class ProjectById(Resource):
    # Маршрут для получения информации о конкретном проекте
    def get(self, project_id):
        project = Project.query.get(project_id)

        if not project:
            return jsonify({'message': 'Project not found'})

        user_infos = []

        for user_id in project.members:
            user = Users.query.get(user_id)
            user_infos.append({'user_id': user.user_id,
                               'name': user.name})

        contact_person_info = Users.query.get(project.contact_person)

        project_data = {
            'project_id': project.project_id,
            'name': project.name,
            'goal': project.goal,
            'members': user_infos,
            'platform': project.platform,
            'contact_person': {'user_id': contact_person_info.user_id,
                               'name': contact_person_info.name}
        }
        return jsonify(project_data)

    # Маршрут для обновления информации о конкретном проекте

    @swagger_api.expect(projects_post)
    def put(self, project_id):
        project = Project.query.get(project_id)
        if not project:
            return jsonify({'message': 'Project not found'})

        project_data = request.get_json()

        project.name = project_data['name']
        project.goal = project_data['goal']
        project.members = project_data['members']
        project.platform = project_data['platform']
        project.contact_person = project_data['contact_person']

        db.session.commit()
        return jsonify({'message': 'Project updated successfully'})

    # Маршрут для удаления конкретного проекта
    def delete(self, project_id):
        project = Project.query.get(project_id)
        if not project:
            return jsonify({'message': 'Project not found'})
        db.session.delete(project)
        db.session.commit()
        return jsonify({'message': 'Project deleted successfully'})


projects_news_post = swagger_api.model('projects_news_post', {
    'title': fields.String(required=True, description=''),
    'content': fields.String(required=True, description=''),
    'timestamp': fields.DateTime(required=True, description='')
})


@swagger_api.route('/projects/<uuid:project_id>/news')
class ProjectNews(Resource):
    # Маршрут для получения списка всех новостей для указанного проекта
    def get(self, project_id):
        project = Project.query.get(project_id)
        if not project:
            return jsonify({'message': 'Project not found'})
        news_list = []
        for news in project.news:
            news_data = {
                'news_id': news.news_id,
                'title': news.title,
                'content': news.content,
                'timestamp': news.timestamp
            }
            news_list.append(news_data)
        return jsonify(news_list)

    # Маршрут для создания новой новости для указанного проекта
    @swagger_api.expect(projects_news_post)
    def post(self, project_id):
        project = Project.query.get(project_id)
        if not project:
            return jsonify({'message': 'Project not found'})
        news_data = request.get_json()
        news = News(
            project_id=project_id,
            title=news_data['title'],
            content=news_data['content'],
            timestamp=parse(news_data['timestamp'])
        )
        db.session.add(news)
        db.session.commit()
        return jsonify({'message': 'News created successfully'})


projects_versions_post = swagger_api.model('projects_versions_post', {
    'version_number': fields.String(required=True, description=''),
    'release_date': fields.DateTime(required=True, description=''),
    'description': fields.String(required=True, description='')
})


@swagger_api.route('/projects/<uuid:project_id>/versions')
class ProjectVersions(Resource):
    # Маршрут для получения списка всех версий разрабатываемой программы для указанного проекта
    def get(self, project_id):
        project = Project.query.get(project_id)
        if not project:
            return jsonify({'message': 'Project not found'})
        version_list = []
        for version in project.versions:
            version_data = {
                'version_id': version.version_id,
                'version_number': version.version_number,
                'release_date': version.release_date,
                'description': version.description
            }
            version_list.append(version_data)
        return jsonify(version_list)

    # Маршрут для создания новой версии разрабатываемой программы для указанного проекта
    @swagger_api.expect(projects_versions_post)
    def post(self, project_id):
        project = Project.query.get(project_id)
        if not project:
            return jsonify({'message': 'Project not found'})
        version_data = request.get_json()
        version = Version(
            project_id=project_id,
            version_number=version_data['version_number'],
            release_date=parse(version_data['release_date']),
            description=version_data['description']
        )
        db.session.add(version)
        db.session.commit()
        return jsonify({'message': 'Version created successfully'})


@swagger_api.route('/projects/<uuid:project_id>/versions/<uuid:version_id>')
class ProjectSelectedVersionsInfo(Resource):
    # Маршрут для получения информации о конкретной версии разрабатываемой программы
    def get(self, project_id, version_id):
        version = Version.query.filter_by(
            project_id=project_id, version_id=version_id).first()
        if not version:
            return jsonify({'message': 'Version not found'})
        version_data = {
            'version_id': version.version_id,
            'version_number': version.version_number,
            'release_date': version.release_date,
            'description': version.description
        }
        return jsonify(version_data)


projects_documentation_post = swagger_api.model('projects_documentation_post', {
    'title': fields.String(required=True, description=''),
    'content': fields.String(required=True, description='')
})


upload_parser = swagger_api.parser()
upload_parser.add_argument('file', location='files',
                           type=FileStorage, required=True)


@swagger_api.route('/projects/<uuid:project_id>/versions/<uuid:version_id>/documentation')
class ProjectSelectedVersionsDocumentation(Resource):
    # Маршрут для получения списка всех документов, связанных с конкретной версией программы
    def get(self, project_id, version_id):
        version = Version.query.filter_by(
            project_id=project_id, version_id=version_id).first()
        if not version:
            return jsonify({'message': 'Version not found'})
        documentation_list = []
        for documentation in version.documentation:
            documentation_data = {
                'doc_id': documentation.doc_id,
                'title': documentation.title,
                'content': documentation.content,
                'files': documentation.files
            }
            documentation_list.append(documentation_data)
        return jsonify(documentation_list)

    # Маршрут для создания нового документа для указанной версии программы
    @swagger_api.expect(projects_documentation_post)
    def post(self, project_id, version_id):
        version = Version.query.filter_by(
            project_id=project_id, version_id=version_id).first()
        if not version:
            return jsonify({'message': 'Version not found'})
        documentation_data = request.get_json()
        documentation = Documentation(
            version_id=version_id,
            title=documentation_data['title'],
            content=documentation_data['content']
        )
        db.session.add(documentation)
        db.session.commit()
        return jsonify({'message': 'Documentation created successfully'})


upload_parser = swagger_api.parser()
upload_parser.add_argument('file', location='files',
                           type=FileStorage, required=True)


@swagger_api.route('/projects/versions/documentation/file/<string:file_name>')
class ProjectSelectedVersionsDocumentationFileDownload(Resource):
    def get(self, file_name):

        full_path = './files/' + file_name

        if not os.path.exists(full_path):
            return jsonify({'message': 'File not found'})

        return send_file(full_path, as_attachment=True)


@swagger_api.route('/projects/versions/documentation/<uuid:doc_id>/file')
class ProjectSelectedVersionsDocumentationFileUpload(Resource):
    @swagger_api.expect(upload_parser)
    def post(self, doc_id):
        documentation = Documentation.query.get(doc_id)
        if not documentation:
            return jsonify({'message': 'Documentation not found'})

        if documentation.files is None:
            documentation.files = []

        args = upload_parser.parse_args()
        uploaded_file = args['file']
        uploaded_file_type = uploaded_file.filename.split('.')[-1]
        filename = str(doc_id) + '_' + str(uuid.uuid4()) + \
            '.' + uploaded_file_type

        savepath = './files/' + filename
        uploaded_file.save(savepath)

        documentation.files.append(savepath)
        flag_modified(documentation, "files")
        db.session.commit()

        return jsonify({'message': 'File created successfully', 'args': str(args['file']), 'wtf': str(request.files['file'])})


projects_bugs_post = swagger_api.model('projects_bugs_post', {
    'title': fields.String(required=True, description=''),
    'description': fields.String(required=True, description=''),
    'status': fields.String(required=True, description='')
})


@swagger_api.route('/projects/<uuid:project_id>/versions/<uuid:version_id>/bugs')
class ProjectSelectedVersionsBugs(Resource):
    # Маршрут для получения списка всех ошибок, связанных с указанным проектом и версией программы
    def get(self, project_id, version_id):
        bugs = Bug.query.filter_by(
            project_id=project_id, version_id=version_id).all()
        bug_list = []
        for bug in bugs:
            bug_data = {
                'bug_id': bug.bug_id,
                'title': bug.title,
                'description': bug.description,
                'status': bug.status
            }
            bug_list.append(bug_data)
        return jsonify(bug_list)

    # Маршрут для создания новой ошибки для указанного проекта и версии программы
    @swagger_api.expect(projects_bugs_post)
    def post(self, project_id, version_id):
        project = Project.query.get(project_id)
        version = Version.query.filter_by(
            project_id=project_id, version_id=version_id).first()
        if not project or not version:
            return jsonify({'message': 'Project or Version not found'})
        bug_data = request.get_json()
        bug = Bug(
            project_id=project_id,
            version_id=version_id,
            title=bug_data['title'],
            description=bug_data['description'],
            status=bug_data['status']
        )
        db.session.add(bug)
        db.session.commit()
        return jsonify({'message': 'Bug created successfully'})


@swagger_api.route('/projects/<uuid:project_id>/details')
class ProjectDetails(Resource):
    def get(self, project_id):
        project = Project.query.get(project_id)
        if not project:
            return jsonify({'message': 'Project not found'})

        user_infos = []
        for user_id in project.members:
            user = Users.query.get(user_id)
            user_infos.append({'user_id': user.user_id,
                               'name': user.name})

        contact_person_info = Users.query.get(project.contact_person)

        project_data = {
            'project_id': project.project_id,
            'name': project.name,
            'goal': project.goal,
            'members': user_infos,
            'platform': project.platform,
            'contact_person': {'user_id': contact_person_info.user_id,
                               'name': contact_person_info.name},
            'news': [],
            'versions': [],
            'bugs': []
        }

        for news in project.news:
            news_data = {
                'news_id': news.news_id,
                'title': news.title,
                'content': news.content,
                'timestamp': news.timestamp
            }
            project_data['news'].append(news_data)

        for version in project.versions:
            version_data = {
                'version_id': version.version_id,
                'version_number': version.version_number,
                'release_date': version.release_date,
                'description': version.description,
                'documentation': []
            }

            for documentation in version.documentation:
                doc_data = {
                    'doc_id': documentation.doc_id,
                    'title': documentation.title,
                    'content': documentation.content
                }
                version_data['documentation'].append(doc_data)

            project_data['versions'].append(version_data)

        for bug in project.bugs:
            bug_data = {
                'bug_id': bug.bug_id,
                'title': bug.title,
                'description': bug.description,
                'status': bug.status
            }
            project_data['bugs'].append(bug_data)

        return jsonify(project_data)


@swagger_api.route('/generate_fake_data')
class FakeDataGen(Resource):
    def get(self):
        for _ in range(1):
            for _ in range(1):
                test_usr = generateUser()

            for _ in range(1):
                test_proj = generateProject()

                for _ in range(1):
                    generateNews(test_proj)
                    rand_version = generateVersions(test_proj)

                    for _ in range(1):
                        generateBugs(test_proj, rand_version)
                        doc = generateDocumentation(test_proj, rand_version)
                        generateFile(doc)

        return jsonify({'message': 'Done'})
